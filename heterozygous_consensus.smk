rule consensus_create:
    input:
        reference=expand("reference/{ref}.fa", ref=config["reference"]),
        vcf="filtered_combined/{sample}.filtered.vcf.gz"
    output:
        fasta=protected("het_consensus/{sample}.fa"),
        part=temp("het_consensus/{sample}.partitions"),
        flat="het_consensus/{sample}.flat"
    threads: 1
    shell:
        "consolidate_ref_vcf.py -r {input.reference} -i {input.vcf} -o {output.fasta} --het --partition --concat"


rule mfa_create:
    input:
        expand("het_consensus/{sample}.flat", sample=config['samples'])
    output:
        expand("het_consensus/{ref}.mfa", ref=config["reference"])
    threads: 1
    shell:
        "cat {input} > {output}"

rule partition_create:
    input:
        expand("het_consensus/{firstsample}.partitions", firstsample=config['samples'][0])
    output:
        expand("het_consensus/{ref}.partitions", ref=config["reference"])
    threads: 1
    shell:
        "cp {input} {output}"


rule tree_inference:
    input:
        fasta=expand("het_consensus/{ref}.mfa", ref=config["reference"]),
        part=expand("het_consensus/{ref}.partitions", ref=config["reference"])
    output:
        expand("het_consensus/{ref}.partitions.treefile", ref=config["reference"])
    threads: 20
    shell:
        "iqtree -s {input.fasta} -spp {input.part} -m TEST -bb 1000 -alrt 1000 -wbt -ntmax 20 -nt AUTO"
