from glob import glob


rule minimap2_indexing:
    input:
        expand("reference/{reference}.fa", reference=config["reference"])
    output:
        "reference/{reference}.idx"
    shell:
        "minimap2 -k 17 -w 10 -d {output} {input}"


rule minimap2_map:
    input:
        reference=expand("reference/{ref}.fa", ref=config["reference"]),
        idx=expand("reference/{ref}.idx", ref=config["reference"]),
        reads=lambda wildcards: glob('raw_data/{sample}*.gz'.format(sample=wildcards.sample))
    output:
        temp("mapped_reads/{sample}.bam")
    params:
        rg=r"@RG\tID:{sample}\tSM:{sample}\tPL:ILLUMINA"
    threads: 8
    shell:
        "minimap2 -K 1G -t 6 -R \"{params.rg}\" -ax sr {input.reference} {input.reads} | samtools view -Sb - > {output}"


rule samtools_sort:
    input:
        "mapped_reads/{sample}.bam"
    output:
        protected("sorted_bams/{sample}.bam")
    threads: 1
    shell:
        "samtools sort -O BAM -o {output} {input}"


rule sambamba_dedup:
    input:
        "sorted_bams/{sample}.bam"
    output:
        protected("deduped_bams/{sample}.bam")
    params:
        tempfolder="/tmp/sambamba_dedup"
    threads: 1
    shell:
        "sambamba markdup --remove-duplicates --tmpdir={params.tempfolder} {input} {output}"


rule samtools_index:
    input:
        "deduped_bams/{sample}.bam"
    output:
        "deduped_bams/{sample}.bam.bai"
    threads: 1
    shell:
        "samtools index {input}"
