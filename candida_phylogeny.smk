
"""
Snakemake pipeline for phylogenetic inference from fungal whole genome
sequencing raw-reads.
Author:
    Judit Szarvas
Affiliation:
    Technical University of Denmark, Division for Global Surveillance,
    Research Group for Genomic Epidemiology
License:
    Apache License, Version 2.0, http://www.apache.org/licenses/LICENSE-2.0
"""

configfile: "config.json"

rule all:
    input:
        bam=expand("deduped_bams/{sample}.bam.bai", sample=config['samples']),
        vcf=expand("filtered_combined/{sample}.filtered.vcf.gz", sample=config["samples"]),
        tree=expand("het_consensus/{reference}.partitions.treefile", reference=config["reference"])

include: "read_mapping.smk"

include: "variant_calling.smk"

include: "heterozygous_consensus.smk"
