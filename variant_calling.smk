def sample_name(longsample):
    #return "-".join(longsample.split("-")[5:])
    return longsample

rule gatk_indexing:
    input:
        "reference/{reference}.fa"
    output:
        "reference/{reference}.fa.fai"
    shell:
        "samtools faidx {input}"

rule gatk_dicting:
    input:
        "reference/{reference}.fa"
    output:
        "reference/{reference}.dict"
    shell:
        "gatk CreateSequenceDictionary -R {input}"

rule gatk_caller:
    input:
        reference=expand("reference/{reference}.fa", reference=config["reference"]),
        bam="deduped_bams/{sample}.bam",
        bai="deduped_bams/{sample}.bam.bai",
        idx=expand("reference/{reference}.fa.fai", reference=config["reference"]),
        sdict=expand("reference/{reference}.dict", reference=config["reference"])
    output:
        "raw_variants/{sample,\w+}.vcf.gz"
    params:
        sn=lambda wildcards: sample_name(wildcards.sample),
        ploidy=config["ploidy"]
    shell:
        "gatk --java-options \"-Xmx4g\" HaplotypeCaller -R {input.reference} -I {input.bam} --sample-name {params.sn} -O {output} --sample-ploidy {params.ploidy} --max-reads-per-alignment-start 0  -stand-call-conf 10.0"


rule gatk_select:
    input:
        reference=expand("reference/{reference}.fa", reference=config["reference"]),
        vcf="raw_variants/{sample}.vcf.gz"
    output:
        "raw_variants/{sample,\w+}.snps.vcf.gz"
    shell:
        "gatk SelectVariants -R {input.reference} -V {input.vcf} -O {output} --select-type-to-include SNP"

rule gatk_filter:
    input:
        reference=expand("reference/{reference}.fa", reference=config["reference"]),
        vcf="raw_variants/{sample}.snps.vcf.gz"
    output:
        "filtered_variants/{sample,\w+}.snps.vcf.gz"
    shell:
        "gatk VariantFiltration -R {input.reference} -V {input.vcf} -O {output} "
        "--filter-name 'L_QD' "
        "--filter-expression 'QD < 2.0' "
        "--filter-name 'H_FS' "
        "--filter-expression 'FS > 60.0' "
        "--filter-name 'H_SOR' "
        "--filter-expression 'SOR > 3.0' "
        "--filter-name 'L_MQ' "
        "--filter-expression 'MQ < 40.0' "
        "--filter-name 'L_MRS' "
        "--filter-expression 'MQRankSum < -10.0'"


rule indel_select:
    input:
        reference=expand("reference/{reference}.fa", reference=config["reference"]),
        vcf="raw_variants/{sample}.vcf.gz"
    output:
        "raw_variants/{sample,\w+}.indels.vcf.gz"
    shell:
        "gatk SelectVariants -R {input.reference} -V {input.vcf} -O {output} --select-type-to-include INDEL"


rule indel_filter:
    input:
        reference=expand("reference/{reference}.fa", reference=config["reference"]),
        vcf="raw_variants/{sample}.indels.vcf.gz"
    output:
        "filtered_variants/{sample,\w+}.indels.vcf.gz"
    shell:
        "gatk VariantFiltration -R {input.reference} -V {input.vcf} -O {output} "
        "--filter-name 'L_QD' "
        "--filter-expression 'QD < 2.0' "
        "--filter-name 'H_FS' "
        "--filter-expression 'FS > 200.0' "
        "--filter-name 'H_SOR' "
        "--filter-expression 'SOR > 10.0' "
        "--filter-name 'L_RPRS' "
        "--filter-expression 'ReadPosRankSum < -20.0'"

rule gatk_merge:
    input:
        snps="filtered_variants/{sample}.snps.vcf.gz",
        indels="filtered_variants/{sample}.indels.vcf.gz"
    output:
        "filtered_combined/{sample,\w+}.filtered.vcf.gz"
    shell:
        "gatk MergeVcfs -I {input.snps} -I {input.indels} -O {output}"
