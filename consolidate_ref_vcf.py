#!/usr/bin/env python3

import sys
import os
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
from cyvcf2 import VCF

def ambigous_dna(dna_letters):
    ambigous_dna_encoding = {
        "A": "A",
        "C": "C",
        "G": "G",
        "T": "T",
        "AG": "R",
        "CT": "Y",
        "CG": "S",
        "AT": "W",
        "GT": "K",
        "AC": "M",
        "CGT": "B",
        "AGT": "D",
        "ACT": "H",
        "ACG": "V",
        "ACGT": "n",
        "": "-"
        }
    return ambigous_dna_encoding["".join(sorted(list(set(dna_letters)))).upper()]

parser = argparse.ArgumentParser(
    description='Consensus sequence from VCF file and reference fasta')
parser.add_argument(
    '-r',
    dest="ref_file",
    default=None,
    help='Input fasta reference')
parser.add_argument(
    '-i',
    dest="vcf_file",
    default=None,
    help='Input VCF.gz')
parser.add_argument(
    '-o',
    dest="ofile",
    default=None,
    help='Output consensus (multi) fasta')
parser.add_argument(
    '--partition',
    dest="part",
    action="store_true",
    help='Create partitions file')
parser.add_argument(
    '--concat',
    dest="concat",
    action="store_true",
    help='Concatenate the entries')
parser.add_argument(
    '--het',
    dest="het",
    action="store_true",
    help='Add heterozygous SNPs')
parser.add_argument(
    '--fill',
    dest="fill",
    action="store_true",
    help='Low confidence variants are replaced with bases of the reference')
parser.add_argument(
    '--indel',
    dest="indel",
    action="store_true",
    help='Allow insertions and deletions into the consensus sequence (not equal length)')
args = parser.parse_args()

vcf_i = None
seq_iter = None
if args.vcf_file is not None and os.path.exists(args.vcf_file):
    vcf_i = VCF(args.vcf_file)
else:
    sys.exit("VCF needed")

if args.ref_file is not None and os.path.exists(args.ref_file):
    records = list(SeqIO.parse(args.ref_file, "fasta"))
else:
    sys.exit("Ref needed")


# references = vcf_i.seqnames
concat = []
contig = []
variant = None
prev_end = 0
#contig_len = 0
lens = []
# change id and description of reference record
sample_id = ""
sample_desc = "Aligned to {}"
if len(vcf_i.samples) == 1:
    sample_id = vcf_i.samples[0]
for rec in records:
    if rec.id in vcf_i.seqnames:
        print(rec.id)
        #print(vcf_i.seqnames)
        print(len(rec.seq))
        try:
            if variant is None:
                variant = next(vcf_i)
            # right contig
            print(variant.CHROM, rec.id)
            while (variant.CHROM == rec.id):
                #print(prev_end, variant.start, variant.end, variant.REF, variant.ALT)
                # overarching deletions
                if prev_end and (variant.start < prev_end or '*' in variant.ALT):
                    variant = next(vcf_i)
                    continue
                contig.append(str(rec.seq[prev_end:variant.start]))
                #contig_len += variant.start - prev_end + len(variant.REF)
                # .end is 1 based, .start is 0
                prev_end = variant.end
                # passed VariantFiltration, and not deletion or struct var
                if variant.FILTER is None:
                    if not variant.is_deletion and not variant.is_sv:
                        # homozygous snps and insertions -- 1:, 1/1:
                        if variant.genotypes[0][0] != 0 and len(set(variant.genotypes[0][:-1])) == 1:
                            if not args.indel:
                                # insertion A -> ACT gets trimmed
                                contig.append(variant.ALT[0][:len(variant.REF)])
                            else:
                                # full length inserts
                                contig.append(variant.ALT[0])
                        # heterozygous snps
                        else:
                            if args.het:
                                b = variant.ALT + [variant.REF]
                                if variant.genotypes[0][0] != 0:
                                    b = variant.ALT
                                if variant.is_snp:
                                    contig.append(ambigous_dna(b))
                                    #print(variant.REF, variant.ALT, variant.POS, b[:2])
                                elif variant.is_indel:
                                    #insert or het del
                                    # encode T -> G,TAAC and TTAG -> T,TTAGTA
                                    b_len = 0
                                    if not args.indel:
                                        # trimmed to REF length
                                        b_len = len(variant.REF)
                                    else:
                                        b_len = max([len(x) for x in b])
                                    start = 0
                                    for r in range(1,b_len+1):
                                        bit_b = [x[start:r] for x in b]
                                        contig.append(ambigous_dna(bit_b))
                                        start = r
                            else:
                                contig.append("n" * len(variant.REF))
                    elif variant.is_deletion:
                        # TTG -> T
                        # first (or preceding) base
                        contig.append(variant.ALT[0][0])
                        #print(contig[-1])
                        if not args.indel:
                            # deletion gets filled with "-"s
                            contig.append("-" * (len(variant.REF) - 1))
                else:
                    if not args.fill:
                        # low conf. variants are n-s
                        contig.append("n" * len(variant.REF))
                    else:
                        # low conf. variants are filled from reference
                        contig.append(variant.REF)

                # read next variant
                variant = next(vcf_i)
            contig.append(str(rec.seq[prev_end:]))
        except StopIteration:
            #print("Run out")
            contig.append(str(rec.seq[prev_end:]))
        rec.seq = Seq("".join(contig))
        rec.id = sample_id
        rec.description = sample_desc.format(rec.description)
        if args.concat:
            concat.append("".join(contig))
        print("Cons len:", len(rec.seq))
        lens.append(len(rec.seq))
        contig = []
        #contig_len = 0
        prev_end = 0


with open(args.ofile, "w") as op:
    SeqIO.write(records, op, "fasta")

if args.concat:
    full_seq = "".join(concat)
    with open("{}.flat".format(args.ofile.rsplit(".",1)[0]), "w") as fop:
        print(">{}".format(sample_id), file=fop)
        for i in range(0, len(full_seq), 60):
            print(full_seq[i:i+60], file=fop)

if args.part:
    cummulated_len = 0
    with open("{}.partitions".format(args.ofile.rsplit(".",1)[0]), "w") as pop:
        for i, part_len in enumerate(lens):
            end_len = cummulated_len + part_len
            print("DNA, part{0} = {1}-{2}".format(i+1, cummulated_len + 1, end_len), file=pop)
            cummulated_len = end_len
