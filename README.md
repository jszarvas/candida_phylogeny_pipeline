Scripts for inferring maximum likelihood phylogenies from whole-genome sequencing data from Candida species.

###### Requirements
- Python 3
 - Biopython version 1.96
 - cyvcf2
 - snakemake
- [minimap2](https://github.com/lh3/minimap2)
- [sambamba](http://lomereiter.github.io/sambamba/)
- [samtools version 1.6](https://github.com/samtools/samtools)
- [IQ-tree version 1.6.12](http://www.iqtree.org)


###### Example
```bash
snakemake -s candida_phylogeny_pipeline/candida_phylogeny.smk --configfile candida_phylogeny_pipeline/config.json
```
